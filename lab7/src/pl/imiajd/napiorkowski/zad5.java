
package pl.imiajd.napiorkowski;

 class Osoba {
    
    private String nazwisko;
    private int rokUrodzenia;
    
    Osoba(String naz, int rokur){
        this.nazwisko = naz;
        this.rokUrodzenia = rokur;
        
    }
    
    public String get_nazwisko()
    {
        return nazwisko;
    }
    
    public int get_rokur()
    {
        return rokUrodzenia;
    }
    
    public String ToString()
    {
        return nazwisko+" "+rokUrodzenia+" ";
    }
    
}
     class Student extends Osoba {
        private String kierunek;

         Student(String naz, int rokur, String kierunek) {
            super(naz, rokur);
            kierunek=null;
            super.get_nazwisko();
            super.get_rokur();
            super.ToString();
        
        }
        public String get_kierunek()
        {
            return kierunek;
        }
        }
     class Nauczyciel extends Osoba{
        private int pensja;  

         Nauczyciel(String naz, int rokur, int pensja) {
            super(naz, rokur);
            pensja = 0;
            super.get_nazwisko();
            super.get_rokur();
            super.ToString();
        }
        public int get_pensja()
        {
            return pensja;
        }
        
        
        }
    


