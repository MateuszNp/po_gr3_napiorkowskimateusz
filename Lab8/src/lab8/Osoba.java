
package lab8;

import java.util.Arrays;

public class Osoba {
    
    private String nazwisko;
    private int rokUrodzenia;
    private String[] imiona;
    private java.time.LocalDate dataUrodzenia;
    private boolean plec;
    
    Osoba(String naz, int rokur,String[] imiona,java.time.LocalDate dataUr ,boolean plec){
        this.nazwisko = naz;
        this.rokUrodzenia = rokur;
        this.imiona = imiona;
        this.dataUrodzenia = dataUr;
        this.plec = plec;
        
        
    }
    
    public String get_Nazwisko()
    {
        return nazwisko;
    }
    
    public int get_Rokur()
    {
        return rokUrodzenia;
    }
    public String[] get_Imiona()
    {
        return imiona;
    }
    public java.time.LocalDate get_DataUrodzenia()
    {
        return dataUrodzenia;
    }
    public boolean get_Plec()
    {
        return plec;
    }
    
    public String ToString()
    {
        return nazwisko+" "+rokUrodzenia+" ";
    }
    
}
     class Student extends Osoba {
        private String kierunek;
        private double sredniaOcen;
        
        
         Student(String naz, int rokur, String kierunek,double sredniaOcen,String[] imiona,java.time.LocalDate dataUr ,boolean plec) {
            super(naz, rokur,imiona,dataUr,plec);
            this.kierunek=kierunek;
            this.sredniaOcen = sredniaOcen;
            super.get_Nazwisko();
            super.get_Rokur();
            super.get_Imiona();
            super.get_DataUrodzenia();
            super.get_Plec();
        }
        public String get_Kierunek()
        {
            return this.kierunek;
        }
        public void set_SredniaOcen(int srednia)
        {
            sredniaOcen=srednia;
        }
        public double get_SredniaOcen()
        {
           return this.sredniaOcen; 
        }
        @Override
        public String ToString()
         {
             return get_Nazwisko()+" "+Arrays.toString(get_Imiona())+" "+get_Rokur()+" "+this.kierunek+" "+get_DataUrodzenia()+" "+get_Plec()+" "+this.sredniaOcen;
         }
        }
     class Nauczyciel extends Osoba{
        private int pensja;  
        private java.time.LocalDate dataZatrudnienia;

         Nauczyciel(String naz, int rokur, int pensja,java.time.LocalDate dataZatrudnienia, String[] imiona,java.time.LocalDate dataUr ,boolean plec ) {
            super(naz, rokur,imiona,dataUr,plec);
            this.pensja = pensja;
            this.dataZatrudnienia = dataZatrudnienia;
            super.get_Nazwisko();
            super.get_Rokur();
            super.get_Imiona();
            super.get_DataUrodzenia();
            super.get_Plec();
            
            
        }
        public int get_Pensja()
        {
            return this.pensja;
        }
        public java.time.LocalDate get_DataZatrudnienia()
        {
            return this.dataZatrudnienia;
        }
        @Override
        public String ToString()
        {
            return get_Nazwisko()+" "+Arrays.toString(get_Imiona())+" "+get_Rokur()+" "+this.pensja+" "+get_DataUrodzenia()+" "+get_Plec()+" "+this.dataZatrudnienia;
        }
        
        }
    
