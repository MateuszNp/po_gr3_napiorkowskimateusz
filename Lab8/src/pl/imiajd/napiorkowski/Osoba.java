
package pl.imiajd.napiorkowski;

import java.util.Arrays;

public class Osoba {
    
    private String nazwisko;
    private int rokUrodzenia;
    private String[] imiona;
    private java.time.LocalDate dataUrodzenia;
    private boolean plec;
    
    Osoba(String naz, int rokur,String[] imiona,java.time.LocalDate dataUr ,boolean plec){
        this.nazwisko = naz;
        this.rokUrodzenia = rokur;
        this.imiona = imiona;
        this.dataUrodzenia = dataUr;
        this.plec = plec;
        
        
    }
    
    public String get_Nazwisko()
    {
        return nazwisko;
    }
    
    public int get_Rokur()
    {
        return rokUrodzenia;
    }
    public String[] get_Imiona()
    {
        return imiona;
    }
    public java.time.LocalDate get_DataUrodzenia()
    {
        return dataUrodzenia;
    }
    public boolean get_Plec()
    {
        return plec;
    }
    
    public String ToString()
    {
        return nazwisko+" "+rokUrodzenia+" ";
    }
    
}
     
     
    
