
package pl.imiajd.napiorkowski;

import java.time.LocalDate;


public class Fortepian extends Instrument {
    Fortepian(String producent, LocalDate rokProdukcji){
        super(producent, rokProdukcji);
    }

    @Override
    String dzwiek() {
        return "brzdęk"; 
    }
    
}
