
package pl.imiajd.napiorkowski;

import java.util.Arrays;


 class Student extends Osoba {
        private String kierunek;
        private double sredniaOcen;
        
        
         Student(String naz, int rokur, String kierunek,double sredniaOcen,String[] imiona,java.time.LocalDate dataUr ,boolean plec) {
            super(naz, rokur,imiona,dataUr,plec);
            this.kierunek=kierunek;
            this.sredniaOcen = sredniaOcen;
            super.get_Nazwisko();
            super.get_Rokur();
            super.get_Imiona();
            super.get_DataUrodzenia();
            super.get_Plec();
        }
        public String get_Kierunek()
        {
            return this.kierunek;
        }
        public void set_SredniaOcen(int srednia)
        {
            sredniaOcen=srednia;
        }
        public double get_SredniaOcen()
        {
           return this.sredniaOcen; 
        }
        @Override
        public String ToString()
         {
             return get_Nazwisko()+" "+Arrays.toString(get_Imiona())+" "+get_Rokur()+" "+this.kierunek+" "+get_DataUrodzenia()+" "+get_Plec()+" "+this.sredniaOcen;
         }
        }