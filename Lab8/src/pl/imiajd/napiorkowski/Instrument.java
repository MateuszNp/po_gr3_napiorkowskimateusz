
package pl.imiajd.napiorkowski;


public abstract class Instrument {
    private String producent;
    private java.time.LocalDate rokProdukcji;
    
    
    Instrument(String producent,java.time.LocalDate rokProdukcji ){
        this.producent = producent;
        this.rokProdukcji = rokProdukcji;
    }
    public String get_Producent()
    {
        return this.producent;
    }
    public java.time.LocalDate get_RokProdukcji()
    {
        return this.rokProdukcji;
    }
    abstract String dzwiek();
    
    @Override
    public String toString()
    {
        return get_Producent()+get_RokProdukcji();
    }
    @Override
    public boolean equals(Object obj) {
    return (Instrument.class == obj);
}
}
