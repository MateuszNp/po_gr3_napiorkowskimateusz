
package pl.imiajd.napiorkowski;

import java.util.Arrays;


class Nauczyciel extends Osoba{
        private int pensja;  
        private java.time.LocalDate dataZatrudnienia;

         Nauczyciel(String naz, int rokur, int pensja,java.time.LocalDate dataZatrudnienia, String[] imiona,java.time.LocalDate dataUr ,boolean plec ) {
            super(naz, rokur,imiona,dataUr,plec);
            this.pensja = pensja;
            this.dataZatrudnienia = dataZatrudnienia;
            super.get_Nazwisko();
            super.get_Rokur();
            super.get_Imiona();
            super.get_DataUrodzenia();
            super.get_Plec();
            
            
        }
        public int get_Pensja()
        {
            return this.pensja;
        }
        public java.time.LocalDate get_DataZatrudnienia()
        {
            return this.dataZatrudnienia;
        }
        @Override
        public String ToString()
        {
            return get_Nazwisko()+" "+Arrays.toString(get_Imiona())+" "+get_Rokur()+" "+this.pensja+" "+get_DataUrodzenia()+" "+get_Plec()+" "+this.dataZatrudnienia;
        }
        
        }
