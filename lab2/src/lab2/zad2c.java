
package lab2;

import java.util.Scanner;
import static lab2.zad2a.generuj;
import static lab2.zad2a.wypisz;


public class zad2c {

    
    public static void main(String args[]) {
          Scanner n = new Scanner (System.in);
        int j = n.nextInt();
        int[] l = new int[j];
        if(j>100 || j<0)
        {
            System.out.println("Liczba jest poza wykresem wprowadz nowa:");
            j=n.nextInt();
        }  
        generuj(l,j,-1000,1000);
        wypisz(l,j);
        System.out.println(ileMaksymalnych(l));
    }
    public static int ileMaksymalnych (int tab[]){
        int max = -1000;
        int ilemax=0;
        for(int i=0;i<tab.length;i++)
        {
            if(max<tab[i])
            {
                max=tab[i];
                ilemax++;
            }
        }
        return ilemax;
    }
}
