
package lab6;


public class Lab6 {

   
    public static void main(String[] args) {
        RachunekBankowy saver1 = new RachunekBankowy();
        RachunekBankowy saver2 = new RachunekBankowy();
        
        saver1.saldo = 2000;
        saver2.saldo = 3000;
        
        RachunekBankowy.setRocznaStopaProcentowa(0.04);
        saver1.obliczMiesieczneOdsetki();
        saver2.obliczMiesieczneOdsetki();
        System.out.println(saver1.saldo);
        System.out.println(saver2.saldo);
        RachunekBankowy.setRocznaStopaProcentowa(0.05);
        saver1.obliczMiesieczneOdsetki();
        saver2.obliczMiesieczneOdsetki();
        System.out.println(saver1.saldo);
        System.out.println(saver2.saldo);
    }

    
}
class RachunekBankowy {
    static double rocznaStopaProcentowa;
     double saldo;
    
     void obliczMiesieczneOdsetki(){
       double s = (saldo * rocznaStopaProcentowa)/12;
       saldo+=s;
    }
     static void setRocznaStopaProcentowa(double stopa){
         rocznaStopaProcentowa = stopa;
     }
}