
package lab12;

import java.util.ArrayList;
import java.util.List;




public class zad2 {

    public static <E> void redukuj( List<E> pracownicy, int n)
    {
        for(int i=0;i<pracownicy.size();i++)
        {
            if(i==n)
            {
                pracownicy.remove(i);
            }
        }
    }
    
    
    public static void main(String args[]) {
        List<String> pracownik = new ArrayList<>();
        pracownik.add("Mick");
        pracownik.add("Nikita");
        pracownik.add("Sebastian");
        pracownik.add("Lance");
        pracownik.add("Max");
        pracownik.add("Sergio");
        pracownik.add("Robert");
        
        System.out.println(pracownik);
        redukuj(pracownik,4);
        
        System.out.println(pracownik);
    }
}
