
package pl.imiajd.napiorkowski;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;

public class zad2 {
   
    public static void main(String args[])
    {
        TestStudent();
    }
   static void TestStudent(){
        LocalDate d = LocalDate.of(1999,8,12);
        Student mirek = new Student("Zkraczny",d, 1);
        Student michal = new Student("kubek",d, 2);
        Student radek = new Student("kotarski",LocalDate.of(1988,2,12),3);
        Student jarek = new Student("kotarski",LocalDate.of(1986,5,18),4);
        Student mateusz = new Student("napiorkowski",LocalDate.of(2001,9,18),5);
        
        ArrayList grupa = new ArrayList();
        grupa.add(mirek);
        grupa.add(michal);
        grupa.add(radek);
        grupa.add(jarek);
        grupa.add(mateusz);
        
        System.out.println(grupa);
        System.out.println("Po sortowaniu: ");
        
       
        Collections.sort(grupa);
        System.out.println(grupa);
    }
}
