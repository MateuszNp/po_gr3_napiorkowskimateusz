
package pl.imiajd.napiorkowski;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Objects;


public class osoba implements Comparable<osoba>{
    private String nazwisko;
    private LocalDate dataUrodzenia;
    osoba(String nazwisko, LocalDate data){
        this.nazwisko = nazwisko;
        this.dataUrodzenia = data;
    }
    @Override
      protected osoba clone() throws CloneNotSupportedException {
          return (osoba) super.clone();
    }      
      
    @Override
    public int compareTo(osoba o) {
        if(this.nazwisko.equals(o.nazwisko))  
            return o.dataUrodzenia.compareTo(this.dataUrodzenia);
        if (this.dataUrodzenia == o.dataUrodzenia)
            return o.nazwisko.compareTo (this.nazwisko);
        
        return 1;
      }
    @Override
      public String toString(){
          return "Osoba"+"["+" "+nazwisko+" "+dataUrodzenia.format(DateTimeFormatter.ISO_DATE)+" "+"]";
      }
    @Override
      public boolean equals(Object d)
      {
          if(this == d)
          {
              return true;
          }
          if(d == null || d.getClass() != this.getClass())
          {
              return false;
          }
          
          osoba os = (osoba) d;
          return (os.nazwisko == this.nazwisko && os.dataUrodzenia == this.dataUrodzenia);
      }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 67 * hash + Objects.hashCode(this.nazwisko);
        hash = 67 * hash + Objects.hashCode(this.dataUrodzenia);
        return hash;
    }
}
