package pl.imiajd.napiorkowski;


import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;

public class zad1 {
   
    public static void main(String args[])
    {
        TestOsoba();
    }
   static void TestOsoba(){
        LocalDate d = LocalDate.of(1999,8,12);
        osoba mirek = new osoba("Zkraczny",d);
        osoba michal = new osoba("kubek",d);
        osoba radek = new osoba("kotarski",LocalDate.of(1988,2,12));
        osoba jarek = new osoba("kotarski",LocalDate.of(1986,5,18));
        osoba mateusz = new osoba("napiorkowski",LocalDate.of(2001,9,18));
        
        ArrayList grupa = new ArrayList();
        grupa.add(mirek);
        grupa.add(michal);
        grupa.add(radek);
        grupa.add(jarek);
        grupa.add(mateusz);
        
        System.out.println(grupa);
        System.out.println("Po sortowaniu: ");
        
       
        Collections.sort(grupa);
        System.out.println(grupa);
    }
}
